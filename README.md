# wiki-metabolic-network

## Description
It is a Docker image, a tool to generate a wiki from a PADMet file. It is useful to explore a metabolic netwok.

## Installation
```sh
docker pull registry.gitlab.inria.fr/dyliss/wiki-metabolic-network-img/wiki-metabolic-network-img:latest
```

## Manual
See this [documentation](https://aureme.readthedocs.io/en/latest/faq.html#how-to-generate-wiki)

## Examples of wikis
[Here](http://aureme.genouest.org/wiki.html)

## Help
Send an email to gem-aureme_AT_inria.fr
